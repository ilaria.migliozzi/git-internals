package core;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.zip.Inflater;


public class GitRepository {
    String path;
    public GitRepository(String path){
        this.path=path;
    }

    public String getHeadRef(){
        String ret="";
        String currentLine;
        BufferedReader br = null;
        FileReader fr = null;

        try{
            fr = new FileReader(path+"/HEAD");
            br = new BufferedReader(fr);

            if((currentLine = br.readLine()) != null)
                ret = currentLine.substring(5);

            if (ret.length()>0)
                return ret;
            else
                return null;
        }catch(IOException e)
        {
            e.printStackTrace();
            return null;
        } finally {

            try {

                if (br != null)
                    br.close();

                if (fr != null)
                    fr.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }

    }

    //Returns hash code 'text' by using SHA1.
    public String getRefHash(String filePath) throws IOException {
        String refPath = this.path+"/"+filePath;
        try (BufferedReader br = new BufferedReader(new FileReader(refPath))) {
            return br.readLine();
        }
    }
}
