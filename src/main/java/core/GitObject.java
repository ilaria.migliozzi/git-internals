package core;

import java.nio.charset.StandardCharsets;

public class GitObject {
    protected String repoPath;
    protected String hash;
    protected String inflatedContent;
    protected byte[] byteArrayInflatedContent;
    protected String name;
    protected String type;
    protected String size;


    GitObject(String repoPath, String hash){
        this.repoPath=repoPath;
        this.hash=hash;
        byteArrayInflatedContent=InflaterGitObjects.inflateObject(repoPath,hash);
        inflatedContent=new String(byteArrayInflatedContent, StandardCharsets.UTF_8);
        this.setType(inflatedContent.substring(0,inflatedContent.indexOf(" ")));
    }



    public String getSize() { return size; }

    public void setSize(String size) { this.size = size; }

    public String getHash() { return hash; }

    public void setName(String name) { this.name = name; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public String getName(){ return name; }

    public GitObject getEntry(String entry){ return this; }
}
