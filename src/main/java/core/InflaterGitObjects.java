package core;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.zip.InflaterInputStream;

public class InflaterGitObjects {

    public static byte[] inflateObject(String repoPath, String hash){
        ByteArrayOutputStream byteArrayInflatedContent=new ByteArrayOutputStream();

        String objPath=repoPath+"/objects/"+hash.substring(0,2)+"/"+hash.substring(2);
        try{
            InputStream in=new FileInputStream(objPath);
            InflaterInputStream iis = new InflaterInputStream(in);

            int b;
            String output="";
            while ((b = iis.read()) != -1) {
                byteArrayInflatedContent.write(b);
                output += (char)b;
            }
            iis.close();
            byteArrayInflatedContent.close();

        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        catch (java.io.IOException e){
            e.printStackTrace();
        }

        return byteArrayInflatedContent.toByteArray();

    }
}
