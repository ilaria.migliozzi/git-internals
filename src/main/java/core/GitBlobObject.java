package core;

public class GitBlobObject extends GitObject {

    private String content;

    public GitBlobObject(String repoPath, String blobHash)  {
        super(repoPath,blobHash);

        this.setType(inflatedContent.substring(0,inflatedContent.indexOf(" ")));

        String size="";
        int i=inflatedContent.indexOf(" ")+1;
        while (inflatedContent.substring(i,i+1).matches("[0-9]")) {
            size += inflatedContent.substring(i,i+1);
            i++;
        }
        this.setSize(size);

        content= inflatedContent.substring(i);

    }

    public String getContent() {
        return content;
    }

    public GitObject getEntry(String entry){ return this; }
}
