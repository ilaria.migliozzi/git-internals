package core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import javax.xml.bind.DatatypeConverter;
import java.util.zip.InflaterInputStream;

public class GitTreeObject extends GitObject {

    private HashMap<String, GitObject> objects;

    public GitTreeObject(String repoPath, String masterTreeHash) {
        super(repoPath,masterTreeHash);

        this.type="tree";

        this.objects= new HashMap<String, GitObject>();

        this.inflateTreeObject();

    }

    public void inflateTreeObject(){
        String contentHash="";
        String contentName="";
        byte[] buffer = new byte[1];

        buffer=byteArrayInflatedContent;

        int j;
        int i=0;

        while(i < inflatedContent.length() && (char) buffer[i] != '\0')
            i++;

        for (i=i+2;i<inflatedContent.length();i++) {
            while ((char) buffer[i] != ' ')
                i++;

            for (j = i + 1; buffer[j] != '\0'; j++) {
                contentName += (char) buffer[j];
            }
            i = j+1;

            byte b20[] = new byte[20];
            for(j = i; j < i + 20; j++)
            {
                b20[j-i] = buffer[j];
            }
            i=j-1;
            contentHash=bytesToHexHash(b20);

            String contentType=new String(InflaterGitObjects.inflateObject(repoPath,contentHash)).substring(0,inflatedContent.indexOf(" "));

            if(contentType.equals("tree")) {
                GitTreeObject gT = new GitTreeObject(repoPath, contentHash);
                objects.put(contentName, gT);
                gT.setName(contentName);
            }
            else if (contentType.equals("blob")) {
                GitBlobObject gB = new GitBlobObject(repoPath, contentHash);
                objects.put(contentName, gB);
                gB.setName(contentName);
            }

            contentName="";
        }
    }

    public String bytesToHexHash(byte[] b)
    {
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[b.length * 2];
        int c;
        for(int j=0; j < b.length; j++)
        {
            c = b[j] & 0xFF;
            hexChars[2*j] = hexArray[c >>> 4];
            hexChars[2*j + 1] = hexArray[c & 0x0F];
        }

        return new String(hexChars).toLowerCase();
    }



    public List<String> getEntryPaths(){
        return new ArrayList<String>(objects.keySet());
    }

    public GitObject getEntry(String entry){
        return objects.getOrDefault(entry, null);
    }

}
