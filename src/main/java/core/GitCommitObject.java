package core;

public class GitCommitObject extends GitObject {

    private String treeHash;
    private String parentHash;
    private String author;

    public GitCommitObject (String repoPath, String masterCommitHash){
        super(repoPath,masterCommitHash);


        int treeIndex=inflatedContent.indexOf("tree");
        int parentIndex=inflatedContent.indexOf("parent");
        int authorIndex=inflatedContent.indexOf("author");
        treeHash=inflatedContent.substring(treeIndex+5,parentIndex-1);

        parentHash = inflatedContent.substring(parentIndex+7,authorIndex-1);

        author=inflatedContent.substring(authorIndex+7,inflatedContent.indexOf(">")+1);

        this.setType("commit");

    }

    public String getTreeHash() { return treeHash; }

    public String getParentHash() { return parentHash; }

    public String getAuthor() { return author; }
}
